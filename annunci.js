// NAVBAR
let containerNav = document.querySelector('#containerNav');
let navbar = document.querySelector('#navbar')
let home = document.querySelector('#home')
let shop = document.querySelector('#shop')
let scroller = document.querySelector('#scroller');

window.addEventListener('scroll', ()=>{
  if(window.scrollY > 0 ){
    navbar.classList.add('bg-main');
    navbar.style.height = '100px';
    containerNav.classList.add('bg-main');

    // home.classList.add('text-acc2')
    // home.classList.remove('text-acc')
    // shop.classList.add('text-acc2')
    // shop.classList.remove('text-acc')
    scroller.classList.remove('d-none')
  } else {
    navbar.classList.remove('bg-main');
    navbar.style.height = '80px';
    containerNav.classList.remove('bg-main');

    // home.classList.remove('text-acc2')
    // home.classList.add('text-acc')
    // shop.classList.remove('text-acc2')
    // shop.classList.add('text-acc')
    scroller.classList.add('d-none')
  }
})



  // DARKMODE

  let btnMode = document.querySelector('#btnMode');
  let iconLight = document.querySelector('#iconLight');
  let iconDark = document.querySelector('#iconDark');

  let clicked = true

  btnMode.addEventListener('click', ()=>{
    iconLight.classList.toggle('d-none');
    iconDark.classList.toggle('d-none');
    if(clicked){
      document.documentElement.style.setProperty('--accent2', 'rgb(38, 38, 38)')
      document.documentElement.style.setProperty('--nero', 'rgb(247, 206, 101)')
      clicked = false
    } else {
      document.documentElement.style.setProperty('--accent2', 'rgb(255, 254, 189)')
      document.documentElement.style.setProperty('--nero','rgb(0, 0, 0)' )
      clicked = true
    }
  })











fetch('./annunci.json').then( (response) => response.json() ).then( (data) =>{

let categoryWrapper = document.querySelector('#categoryWrapper');
let cardsWrapper = document.querySelector('#cardsWrapper');
let priceInput = document.querySelector('#priceInput');
let incrementNumber = document.querySelector('#incrementNumber');
let wordInput = document.querySelector('#wordInput')

function setCategoryFilters() {

    let categories = data.map( (annuncio) => annuncio.category );

    let uniqueCategories = [];

    categories.forEach((category) => {

        if (!uniqueCategories.includes(category)) {

            uniqueCategories.push(category);

        }

    })

    uniqueCategories.forEach((category) =>{

        let div = document.createElement('div');

        div.classList.add('form-check');

        div.innerHTML = `

            <input class="form-check-input" type="radio" name="flexRadioDefault" id="${category}">
                <label class="form-check-label" for="${category}">
                ${category}
             </label>

        `
        categoryWrapper.appendChild(div);

    })

    }

    setCategoryFilters();

    function showCards(array){

        array.sort((a, b)=> b.price - a.price)
        cardsWrapper.innerHTML = '';

        array.forEach( (element) => {

            let div = document.createElement('div');

            div.classList.add('announcement-card');

            div.classList.add('text-center');
            // <div class="cardAnnunci">
            // <img src="${element.image}" alt="" class="img-annunci">
            // </div>
            div.innerHTML = `

            <div class="cardAnnunci">
            <img src="${element.image}" alt="" class="img-annunci">
            </div>
            <p class="h3"> ${element.name} </p>
            <p class="h3"> ${element.category} </p>
            <p class="h3"> ${element.price} <i class="fa-solid fa-coins text-end"></i>
            </p>

            `

            cardsWrapper.appendChild(div);

        })

    }

    showCards(data);



    function filteredByCategory(array) {
        let arrayFromNodeList = Array.from(checkInputs);
        let button = arrayFromNodeList.find((button)=> button.checked);
        let categoria = button.id
        if (categoria != 'All') {

            let filtered = array.filter( (annuncio)=> annuncio.category == categoria );

            return filtered;

        } else {

            return data

        }

    }


    let checkInputs = document.querySelectorAll('.form-check-input');

    checkInputs.forEach( (checkInput) => {

        checkInput.addEventListener('click', ()=>{

            globalFilter();

    })

} )

// evento input prezzo
priceInput.addEventListener('input', ()=>{

  incrementNumber.innerHTML = priceInput.value
  globalFilter();
})

// evento parola

    wordInput.addEventListener('input', ()=>{
     globalFilter();

  })

// SETTARE PRICEINPUT

function setPriceInput(){
  let prices = data.map((annuncio)=> Number(annuncio.price));
  let maxPrice = Math.max(...prices);

  priceInput.max = Math.ceil(maxPrice);
  priceInput.value = Math.ceil(maxPrice);
  incrementNumber.innerHTML = Math.ceil(maxPrice);
}

setPriceInput();

function filteredByPrice(array){
  let filtered = array.filter((annuncio)=> Number(annuncio.price) <= priceInput.value)
  console.log(filtered);
  return filtered
}

// filtro per parola

function filteredByWord(array){
  let parola = wordInput.value;
  let filtered = array.filter((annuncio)=> annuncio.name.toLowerCase().includes(parola.toLowerCase()))
  return filtered
}

// filtro globale
function globalFilter(){
  let filterByCategory = filteredByCategory(data);
  let filterByPrice = filteredByPrice(filterByCategory);
  let filterByWord = filteredByWord(filterByPrice);
  showCards(filterByWord);
}
globalFilter();
})