// NAVBAR
let containerNav = document.querySelector('#containerNav');
let navbar = document.querySelector('#navbar')
let home = document.querySelector('#home')
let shop = document.querySelector('#shop')
let scroller = document.querySelector('#scroller');

window.addEventListener('scroll', ()=>{
  if(window.scrollY > 0 ){
    navbar.classList.add('bg-main');
    navbar.style.height = '100px';
    containerNav.classList.add('bg-main');

    // home.classList.add('text-acc2')
    // home.classList.remove('text-acc')
    // shop.classList.add('text-acc2')
    // shop.classList.remove('text-acc')
    scroller.classList.remove('d-none')
  } else {
    navbar.classList.remove('bg-main');
    navbar.style.height = '80px';
    containerNav.classList.remove('bg-main');

    // home.classList.remove('text-acc2')
    // home.classList.add('text-acc')
    // shop.classList.remove('text-acc2')
    // shop.classList.add('text-acc')
    scroller.classList.add('d-none')
  }
})

// Contatore numeri
function createInterval(finalNumber, element) {

  let counter = 0;

  let trueFinal = finalNumber * 0.999

  let interval = setInterval(()=>{

    if (counter < trueFinal) {

      counter = counter + 3

      element.innerHTML = counter;

    } else if (counter >= trueFinal && counter < finalNumber){

      counter++

      element.innerHTML = counter;

    } else {

      clearInterval(interval);

    }

  }, 1);

}



let first_span = document.querySelector('#first-span');

let second_span = document.querySelector('#second-span');

let third_span = document.querySelector('#third-span');

let h2Intersect = document.querySelector('#stragiIntersect');

let intersectionInterval = true;

let observer = new IntersectionObserver(

  (entries) => {

    entries.forEach((entry) =>{

      if(entry.isIntersecting && intersectionInterval){

        createInterval(3423 , first_span);
        createInterval(6342 , second_span);
        createInterval(10335 , third_span);
        intersectionInterval = false

      }

    })

  }

  )

  observer.observe(h2Intersect);


  // <!-- SWIPER FUNZIONAMENTO  -->


  // creazione dati cards

  let reviews = [

    {name: 'Valerio' , quote: 'Il sito più bello che abbia mai visto'},
    {name: 'Cristina' , quote: 'il sito è di natale ma siamo a Gennaio'},
    {name: 'Verio' , quote: 'da i-phone è illeggibile, orrore!'},
    {name: 'Adam' , quote: 'ci vorrebbero più caroselli!1!1'},

  ]

  // catturiamo il papone wrapper

  let swiperWrapper = document.querySelector('.swiper-wrapper');

  // per ogni reviews crea le card...

  reviews.forEach( (recensione)=>{

    // crea un div

    let div = document.createElement('div');

    // al div creato, aggiungi le classi

    div.classList.add('swiper-slide' , 'd-flex' , 'align-items-center' , 'justify-content-center');

    // riempiamo l'inner HTML del div creato

    div.innerHTML = `

    <div class="card-custom">
    <p class="display-6 seves"> ${recensione.name} </p>

    <p class="text-center"> ${recensione.quote}</p>
    </div>

    `

    // appendere tutto al papone wrapper capo banda

    swiperWrapper.appendChild(div);

  })

  // inizializzazione swiper

  const swiper = new Swiper('.swiper', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,

    // AUTOPLAY

    autoplay: {
      delay: 2500,
      disableOnInteraction: false,
    },

    // EFFECT

    effect: "coverflow",
    grabCursor: true,
    centeredSlides: true,
    slidesPerView: 1,
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: false,
    },

    // If we need pagination
    pagination: {
      el: '.swiper-pagination',
    },

    //  BREAKPOINTS

    breakpoints: {
      640: {
        slidesPerView: 3,
        spaceBetween: 20,
      },
    },

    // Navigation arrows
    // navigation: {
    //   nextEl: '.swiper-button-next',
    //   prevEl: '.swiper-button-prev',
    // },

    // And if we need scrollbar
    // scrollbar: {
    //   el: '.swiper-scrollbar',
    // },
  });

  // DARKMODE

  let btnMode = document.querySelector('#btnMode');
  let iconLight = document.querySelector('#iconLight');
  let iconDark = document.querySelector('#iconDark');

  let clicked = true

  btnMode.addEventListener('click', ()=>{
    iconLight.classList.toggle('d-none');
    iconDark.classList.toggle('d-none');
    if(clicked){
      document.documentElement.style.setProperty('--accent2', 'rgb(38, 38, 38)')
      document.documentElement.style.setProperty('--nero', 'rgb(247, 206, 101)')
      clicked = false
    } else {
      document.documentElement.style.setProperty('--accent2', 'rgb(255, 254, 189)')
      document.documentElement.style.setProperty('--nero','rgb(0, 0, 0)' )
      clicked = true
    }
  })


  